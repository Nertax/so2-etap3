//
// Created by daniel on 02.06.19.
//

#ifndef ETAP_3_COMMONRESOURCES_H
#define ETAP_3_COMMONRESOURCES_H


#include <mutex>
#include <random>
#include <queue>
#include "StudentStatistics.h"

class CommonResources {

public:
    static std::mutex dishesMutex;
    static std::mutex cleanPlatesMutex;
    static std::mutex wetPlatesMutex;
    static std::mutex dirtyPlatesMutex;
    static std::mutex studentStatsMutex;
    static std::mutex tablesMutex;
    static uint dishesAmount;
    static uint cleanPlatesAmount;
    static uint wetPlatesAmount;
    static uint dirtyPlatesAmount;
    static std::default_random_engine generator;
    static std::priority_queue<StudentStatistics> studentsStats;
    static std::vector<int> tables;

};


#endif //ETAP_3_COMMONRESOURCES_H
