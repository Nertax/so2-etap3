//
// Created by daniel on 02.06.19.
//

#ifndef ETAP_3_STUDENTSTATISTICS_H
#define ETAP_3_STUDENTSTATISTICS_H


#include <condition_variable>
#include <mutex>

struct StudentStatistics {

    uint mealsAmount {0};
    std::condition_variable* waitForMeal;


    bool operator<(const StudentStatistics& a) const {
        return mealsAmount > a.mealsAmount;
    }

};


#endif //ETAP_3_STUDENTSTATISTICS_H
