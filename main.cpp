#include <iostream>
#include <ncurses.h>
#include <thread>
#include "Cook.h"
#include "CommonResources.h"
#include "StatusOfThreads.h"
#include "Washer.h"
#include "DinnerLady.h"
#include "Student.h"


int main(int argc, char **argv) {

    int studentAmount;
    int tablesAmount;


    if (argc != 3) {

        std::cout << "Podaj liczbe uczniow oraz liczbe stolow" << std::endl;
        return 3;
    }
    else {

        std::string str(argv[1]);
        studentAmount = std::stoi(str);

        std::string str2(argv[2]);
        tablesAmount = std::stoi(str2);
    }


    if(studentAmount <= 2) {

        std::cout << "Podales liczbe watkow mniejsza od 2!" << std::endl;
        return 1;
    }

    if(studentAmount >= 50) {

        std::cout << "Zbyt duza liczba watkow, podaj liczbe mniejsza niz 50" << std::endl;
        return 2;
    }

    //inicjalizacja ncurses
    initscr();
    cbreak();
    noecho();
//    raw();
    keypad(stdscr, TRUE);
    nodelay(stdscr, TRUE);



    std::thread cookThread = std::thread(Cook::CookDishes);
    std::thread washerThread = std::thread(Washer::Wash);
    std::thread dinnerLadyThread = std::thread(DinnerLady::GiveMeals);



    CommonResources::tables.resize(tablesAmount, -1);

    StatusOfThreads::CurrentActionOfStudents.resize(studentAmount);
    StatusOfThreads::CurrentActionProgressOfStudents.resize(studentAmount);
    StatusOfThreads::CurrentMealCounterOfStudents.resize(studentAmount);

    std::vector<std::thread> threads(studentAmount);
    for(int i = 0; i < studentAmount; i++) {
        threads[i] = std::thread(Student::StudentLife, i);
    }


    //glowna petla programu
    char c = 0;
    while(c != 'q')
    {
        clear();

        c = getch();


        CommonResources::studentStatsMutex.lock();
        mvprintw(3, 5, "Wielkosc kolejki %d",
                 CommonResources::studentsStats.size());
        CommonResources::studentStatsMutex.unlock();


        switch (StatusOfThreads::CurrentCookAction) {

            case CookAction::Cook:
                mvprintw(4, 5, "Watek kucharza: gotuje - %d%%",
                         StatusOfThreads::CurrentCookActionProgress);
                break;

            case CookAction::WaitToDeliverDishes:
                mvprintw(4, 5, "Watek kucharza: czeka aby dostarczyc posilki");
                break;
        }

        switch (StatusOfThreads::CurrentWasherAction) {

            case WasherAction::WaitForDirtyPlates:
                mvprintw(5, 5, "Watek pomywacza (jadl %d razy): czeka na brudne naczynia",
                         StatusOfThreads::CurrentMealCounterOfWasher);
                break;

            case WasherAction::Wash:
                mvprintw(5, 5, "Watek pomywacza (jadl %d razy): zmywa",
                         StatusOfThreads::CurrentMealCounterOfWasher);
                break;

            case WasherAction::WaitToDeliverCleanPlates:
                mvprintw(5, 5, "Watek pomywacza (jadl %d razy): czeka na dostarczenie czystych naczyn",
                         StatusOfThreads::CurrentMealCounterOfWasher);
                break;

            case WasherAction::Break:
                mvprintw(5, 5, "Watek pomywacza (jadl %d razy): przerwa - %d%%",
                         StatusOfThreads::CurrentMealCounterOfWasher, StatusOfThreads::CurrentWasherActionProgress);
                break;

            case WasherAction::Hungry:
                mvprintw(5, 5, "Watek pomywacza (jadl %d razy): glodny",
                         StatusOfThreads::CurrentMealCounterOfWasher);
                break;

            case WasherAction::WaitForFreeTable:
                mvprintw(5, 5, "Watek pomywacza (jadl %d razy): czeka na stolik - %d%%",
                         StatusOfThreads::CurrentMealCounterOfWasher, StatusOfThreads::CurrentWasherActionProgress);
                break;

            case WasherAction::Eat:
                mvprintw(5, 5, "Watek pomywacza (jadl %d razy): je - %d%%",
                         StatusOfThreads::CurrentMealCounterOfWasher, StatusOfThreads::CurrentWasherActionProgress);
                break;
        }

        switch (StatusOfThreads::CurrentDinnerLadyAction) {

            case DinnerLadyAction::WaitForCleanPlatesOrDishes:
                mvprintw(6, 5, "Watek wydawki: czeka na czyste naczynia lub posilki lub glodnych");
                break;

            case DinnerLadyAction::GiveMeals:
                mvprintw(6, 5, "Watek wydawki: wydaje posilki");
                break;

        }

        mvprintw(7, 5, "Liczba posilkow: ");
        CommonResources::dishesMutex.lock();
        printw("%d", CommonResources::dishesAmount);
        CommonResources::dishesMutex.unlock();

        mvprintw(8, 5, "Liczba brudnych naczyn: ");
        CommonResources::dirtyPlatesMutex.lock();
        printw("%d", CommonResources::dirtyPlatesAmount);
        CommonResources::dirtyPlatesMutex.unlock();

        mvprintw(9, 5, "Liczba mokrych naczyn: ");
        CommonResources::wetPlatesMutex.lock();
        printw("%d", CommonResources::wetPlatesAmount);
        CommonResources::wetPlatesMutex.unlock();

        mvprintw(10, 5, "Liczba czystych naczyn: ");
        CommonResources::cleanPlatesMutex.lock();
        printw("%d", CommonResources::cleanPlatesAmount);
        CommonResources::cleanPlatesMutex.unlock();

        for(int i = 0; i < studentAmount; i++) {

            switch (StatusOfThreads::CurrentActionOfStudents[i]) {

                case StudentAction::NotHungry:
                    mvprintw(11 + i, 5, "Student %d (jadl %d razy): Nie jest jeszcze glodny - %d%%", i,
                             StatusOfThreads::CurrentMealCounterOfStudents[i],
                             StatusOfThreads::CurrentActionProgressOfStudents[i]);
                    break;

                case StudentAction::Hungry:
                    mvprintw(11 + i, 5, "Student %d (jadl %d razy): Jest glodny, czeka na posilek", i,
                             StatusOfThreads::CurrentMealCounterOfStudents[i]);
                    break;

                case StudentAction::Eat:
                    mvprintw(11 + i, 5, "Student %d (jadl %d razy): je - %d%%", i,
                             StatusOfThreads::CurrentMealCounterOfStudents[i],
                             StatusOfThreads::CurrentActionProgressOfStudents[i]);
                    break;

                case StudentAction::WaitToPutOffDirtyPlate:
                    mvprintw(11 + i, 5,
                             "Student %d (jadl %d razy): czeka az bedzie mial gdzie odniesc brudny talerz", i,
                             StatusOfThreads::CurrentMealCounterOfStudents[i]);
                    break;

                case StudentAction::WaitForFreeTable:
                    mvprintw(11 + i, 5, "Student %d (jadl %d razy): czeka na wolny stolik", i,
                             StatusOfThreads::CurrentMealCounterOfStudents[i]);
                    break;
            }


        }

        for(int i = 0; i < tablesAmount; i++) {
            if(CommonResources::tables[i] == -1)
            {
                mvprintw(11 + i + studentAmount, 5, "Stolik %d: ", i);
            }
            else if(CommonResources::tables[i] == -2)
            {
                mvprintw(11 + i + studentAmount, 5, "Stolik %d: Pomywacz", i);
            }
            else
            {
                mvprintw(11 + i + studentAmount, 5, "Stolik %d: Student %d", i, CommonResources::tables[i]);
            }
        }

        refresh();
        std::this_thread::sleep_for(std::chrono::milliseconds(17));

    }

    StatusOfThreads::endKeyPressed = true;

    while(CommonResources::studentsStats.size() > 0) {
        StudentStatistics st = CommonResources::studentsStats.top();
        st.waitForMeal->notify_one();
        CommonResources::studentsStats.pop();
    }



    cookThread.join();
    washerThread.join();
    dinnerLadyThread.join();

    for(int i = 0; i < studentAmount; i++) {
        threads[i].join();
    }


    endwin();

    return 0;
}