//
// Created by daniel on 02.06.19.
//

#include <thread>
#include <random>
#include "Cook.h"
#include "StatusOfThreads.h"
#include "CommonResources.h"

void Cook::CookDishes()
{
    std::uniform_int_distribution<int> distribution(6500, 8500);

    while (StatusOfThreads::endKeyPressed == false)
    {
        long timeForAction = distribution(CommonResources::generator);
        StatusOfThreads::CurrentCookAction = CookAction::Cook;

        StatusOfThreads::CurrentCookActionProgress = 0;

        uint a = 10;
        for(int i = 0; i < a; i++)
        {
            StatusOfThreads::CurrentCookActionProgress = ( i * 100 / a );
            std::this_thread::sleep_for(std::chrono::milliseconds( timeForAction  / a));

        }

        StatusOfThreads::CurrentCookActionProgress = 100;
        StatusOfThreads::CurrentCookAction = CookAction::WaitToDeliverDishes;

        CommonResources::dishesMutex.lock();
        CommonResources::dishesAmount += 5;
        CommonResources::dishesMutex.unlock();

    }
}