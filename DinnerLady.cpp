//
// Created by daniel on 02.06.19.
//

#include <thread>
#include "DinnerLady.h"
#include "CommonResources.h"
#include "StatusOfThreads.h"

void DinnerLady::GiveMeals() {

    while (StatusOfThreads::endKeyPressed == false) {

        StatusOfThreads::CurrentDinnerLadyAction = DinnerLadyAction::WaitForCleanPlatesOrDishes;

        while (StatusOfThreads::endKeyPressed == false) {
            CommonResources::cleanPlatesMutex.lock();
            CommonResources::dishesMutex.lock();
            CommonResources::studentStatsMutex.lock();
            if (CommonResources::cleanPlatesAmount > 0 && CommonResources::dishesAmount > 0 && CommonResources::studentsStats.size() > 0) {
                CommonResources::cleanPlatesMutex.unlock();
                CommonResources::dishesMutex.unlock();
                CommonResources::studentStatsMutex.unlock();
                break;
            } else {
                CommonResources::cleanPlatesMutex.unlock();
                CommonResources::dishesMutex.unlock();
                CommonResources::studentStatsMutex.unlock();
            }
        }

        StatusOfThreads::CurrentDinnerLadyAction = DinnerLadyAction::GiveMeals;


        while (StatusOfThreads::endKeyPressed == false) {
            CommonResources::cleanPlatesMutex.lock();
            CommonResources::dishesMutex.lock();
            CommonResources::studentStatsMutex.lock();
            if (CommonResources::cleanPlatesAmount > 0 && CommonResources::dishesAmount > 0 && CommonResources::studentsStats.size() > 0) {

                CommonResources::cleanPlatesAmount--;
                CommonResources::dishesAmount--;

                StudentStatistics st = CommonResources::studentsStats.top();
                st.waitForMeal->notify_one();
                CommonResources::studentsStats.pop();


                CommonResources::studentStatsMutex.unlock();
                CommonResources::cleanPlatesMutex.unlock();
                CommonResources::dishesMutex.unlock();


            } else {
                CommonResources::cleanPlatesMutex.unlock();
                CommonResources::dishesMutex.unlock();
                CommonResources::studentStatsMutex.unlock();
                break;
            }


            std::this_thread::sleep_for(std::chrono::milliseconds(1000));

        }
    }

}
