//
// Created by daniel on 02.06.19.
//

#include "CommonResources.h"

uint CommonResources::dishesAmount = 0;
uint CommonResources::cleanPlatesAmount = 15;
uint CommonResources::wetPlatesAmount = 0;
uint CommonResources::dirtyPlatesAmount = 0;
std::default_random_engine CommonResources::generator;
std::mutex CommonResources::dishesMutex;
std::mutex CommonResources::cleanPlatesMutex;
std::mutex CommonResources::wetPlatesMutex;
std::mutex CommonResources::dirtyPlatesMutex;
std::mutex CommonResources::studentStatsMutex;
std::mutex CommonResources::tablesMutex;
std::priority_queue<StudentStatistics> CommonResources::studentsStats;
std::vector<int> CommonResources::tables;
