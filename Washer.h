//
// Created by daniel on 02.06.19.
//

#ifndef ETAP_3_WASHER_H
#define ETAP_3_WASHER_H

#include "CommonResources.h"
#include "StatusOfThreads.h"
#include "Time.hpp"
#include <thread>
#include <random>

class Washer {
public:
    static void Wash();

};


#endif //ETAP_3_WASHER_H
