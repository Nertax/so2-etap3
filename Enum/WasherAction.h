//
// Created by daniel on 02.06.19.
//

#ifndef ETAP_3_WASHERACTION_H
#define ETAP_3_WASHERACTION_H

enum class WasherAction {
    Wash,
    WaitToDeliverCleanPlates,
    WaitForDirtyPlates,
    Break,
    Hungry,
    Eat,
    WaitForFreeTable

};

#endif //ETAP_3_WASHERACTION_H
