//
// Created by daniel on 02.06.19.
//

#ifndef ETAP_3_STUDENTACTION_H
#define ETAP_3_STUDENTACTION_H

enum class StudentAction {
    NotHungry,
    Hungry,
    Eat,
    WaitToPutOffDirtyPlate,
    WaitForFreeTable

};


#endif //ETAP_3_STUDENTACTION_H
