//
// Created by daniel on 02.06.19.
//

#ifndef ETAP_3_DINNERLADYACTION_H
#define ETAP_3_DINNERLADYACTION_H

enum class DinnerLadyAction {
    GiveMeals,
    WaitForCleanPlatesOrDishes
};

#endif //ETAP_3_DINNERLADYACTION_H
