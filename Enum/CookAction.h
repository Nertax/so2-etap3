//
// Created by daniel on 02.06.19.
//

#ifndef ETAP_3_COOKACTION_H
#define ETAP_3_COOKACTION_H

enum class CookAction {
    Cook,
    WaitToDeliverDishes

};

#endif //ETAP_3_COOKACTION_H
