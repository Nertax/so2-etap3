//
// Created by daniel on 02.06.19.
//

#include <random>
#include <thread>
#include "Student.h"
#include "StudentStatistics.h"
#include "StatusOfThreads.h"
#include "CommonResources.h"

void Student::StudentLife(uint studentNumber) {

    std::uniform_int_distribution<int> distribution(3500, 4500);
    StudentStatistics stat;
    stat.waitForMeal = new std::condition_variable();


    while (StatusOfThreads::endKeyPressed == false) {

        //glodny
        StatusOfThreads::CurrentActionOfStudents[studentNumber] = StudentAction::Hungry;
        std::unique_lock<std::mutex> lck(CommonResources::studentStatsMutex);

        if(StatusOfThreads::endKeyPressed == false)
        {
            CommonResources::studentsStats.push(stat);
        }
        stat.waitForMeal->wait(lck);
        CommonResources::studentStatsMutex.unlock();

        if(StatusOfThreads::endKeyPressed == true)
        {
            return;
        }

        //dostal posilek wiec szuka stolkia
        StatusOfThreads::CurrentActionOfStudents[studentNumber] = StudentAction::WaitForFreeTable;
        int numberOfTable = -1;
        while(numberOfTable == -1 && StatusOfThreads::endKeyPressed == false)
        {
            CommonResources::tablesMutex.lock();

            for (int i = 0; i < CommonResources::tables.size(); ++i) {
                if(CommonResources::tables[i] == -1)
                {
                    numberOfTable = i;
                    CommonResources::tables[i] = studentNumber;
                    break;
                }
            }

            CommonResources::tablesMutex.unlock();
            std::this_thread::sleep_for(std::chrono::milliseconds(30));
        }

        if(StatusOfThreads::endKeyPressed == true)
        {
            return;
        }


        //jak juz usiadl to je
        StatusOfThreads::CurrentActionOfStudents[studentNumber] = StudentAction::Eat;
        long timeForAction = distribution(CommonResources::generator);
        StatusOfThreads::CurrentActionProgressOfStudents[studentNumber] = 0;

        uint a = 10;
        for(int i = 0; i < a; i++)
        {
            StatusOfThreads::CurrentActionProgressOfStudents[studentNumber] = ( i * 100 / a );
            std::this_thread::sleep_for(std::chrono::milliseconds( timeForAction  / a));

        }

        StatusOfThreads::CurrentActionProgressOfStudents[studentNumber] = 100;

        stat.mealsAmount++;

        StatusOfThreads::CurrentMealCounterOfStudents[studentNumber] = stat.mealsAmount;

        //odloz talerz
        StatusOfThreads::CurrentActionOfStudents[studentNumber] = StudentAction::WaitToPutOffDirtyPlate;
        bool isPlaceToPutOffDirtyPlate = false;
        while(!isPlaceToPutOffDirtyPlate && StatusOfThreads::endKeyPressed == false)
        {
            CommonResources::dirtyPlatesMutex.lock();
            if(CommonResources::dirtyPlatesAmount < 5)
            {
                isPlaceToPutOffDirtyPlate = true;
            }
            CommonResources::dirtyPlatesMutex.unlock();
        }

        if(StatusOfThreads::endKeyPressed == true)
        {
            return;
        }

        //skoro ma gdzie odlozyc talerz, to dopiero teraz  wstaje od stolika
        CommonResources::tablesMutex.lock();
        CommonResources::tables[numberOfTable] = -1;
        CommonResources::tablesMutex.unlock();

        //no i odklada talerz
        CommonResources::dirtyPlatesMutex.lock();
        CommonResources::dirtyPlatesAmount++;
        CommonResources::dirtyPlatesMutex.unlock();




        //nie glodny
        StatusOfThreads::CurrentActionOfStudents[studentNumber] = StudentAction::NotHungry;
        timeForAction = distribution(CommonResources::generator);
        StatusOfThreads::CurrentActionProgressOfStudents[studentNumber] = 0;

        a = 10;
        for(int i = 0; i < a; i++)
        {
            StatusOfThreads::CurrentActionProgressOfStudents[studentNumber] = ( i * 100 / a );
            std::this_thread::sleep_for(std::chrono::milliseconds( timeForAction  / a));

        }

        StatusOfThreads::CurrentActionProgressOfStudents[studentNumber] = 100;

    }
}