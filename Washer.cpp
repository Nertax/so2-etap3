//
// Created by daniel on 02.06.19.
//


#include "Washer.h"


void Washer::Wash() {

  //  std::uniform_int_distribution<int> distribution(12500, 14500);
    StudentStatistics stat;
    stat.waitForMeal = new std::condition_variable();

    while (StatusOfThreads::endKeyPressed == false)
    {
   //     long timeForAction = distribution(CommonResources::generator);
        StatusOfThreads::CurrentWasherAction = WasherAction::WaitForDirtyPlates;


        while(StatusOfThreads::endKeyPressed == false)
        {
            CommonResources::dirtyPlatesMutex.lock();
            if(CommonResources::dirtyPlatesAmount > 0)
            {
                CommonResources::dirtyPlatesMutex.unlock();
                break;
            }
            else
            {
                CommonResources::dirtyPlatesMutex.unlock();
            }
        }


        StatusOfThreads::CurrentWasherAction = WasherAction::Wash;


        while(StatusOfThreads::endKeyPressed == false)
        {
            CommonResources::dirtyPlatesMutex.lock();
            if(CommonResources::dirtyPlatesAmount > 0)
            {

                CommonResources::wetPlatesMutex.lock();

                StatusOfThreads::CurrentWasherActionProgress = 0;

                CommonResources::dirtyPlatesAmount--;
                CommonResources::wetPlatesAmount++;



                CommonResources::dirtyPlatesMutex.unlock();
                CommonResources::wetPlatesMutex.unlock();

            }
            else
            {
                CommonResources::dirtyPlatesMutex.unlock();
                break;
            }


            std::this_thread::sleep_for(std::chrono::milliseconds( 1000));

        }

        StatusOfThreads::CurrentWasherActionProgress = 100;

        StatusOfThreads::CurrentWasherAction = WasherAction::WaitToDeliverCleanPlates;

        CommonResources::wetPlatesMutex.lock();
        CommonResources::cleanPlatesMutex.lock();

        CommonResources::cleanPlatesAmount += CommonResources::wetPlatesAmount;
        CommonResources::wetPlatesAmount = 0;


        CommonResources::wetPlatesMutex.unlock();
        CommonResources::cleanPlatesMutex.unlock();



       // StatusOfThreads::CurrentWasherAction = WasherAction::Break;
      //  std::this_thread::sleep_for(std::chrono::milliseconds( timeForAction));

        //glodny
        StatusOfThreads::CurrentWasherAction = WasherAction::Hungry;
        std::unique_lock<std::mutex> lck(CommonResources::studentStatsMutex);

        if(StatusOfThreads::endKeyPressed == false)
        {
            CommonResources::studentsStats.push(stat);
        }

        if(stat.waitForMeal->wait_for(lck, std::chrono::seconds(10)) == std::cv_status::no_timeout)
        {
            if(StatusOfThreads::endKeyPressed == true)
            {
                return;
            }

            //dostala posilek
            StatusOfThreads::CurrentWasherAction = WasherAction::WaitForFreeTable;

            struct timespec a, b;
            clock_gettime(CLOCK_THREAD_CPUTIME_ID, &a);
            clock_gettime(CLOCK_THREAD_CPUTIME_ID, &b);

            int numberOfTable = -1;
            while(numberOfTable == -1 && Time::TimeDiff(a, b) < (2 * 1000000000))
            {
                CommonResources::tablesMutex.lock();

                for (int i = 0; i < CommonResources::tables.size(); ++i) {
                    if(CommonResources::tables[i] == -1)
                    {
                        numberOfTable = i;
                        CommonResources::tables[i] = -2;
                        break;
                    }
                }

                CommonResources::tablesMutex.unlock();
                std::this_thread::sleep_for(std::chrono::milliseconds( 10));
                clock_gettime(CLOCK_THREAD_CPUTIME_ID, &b);
                StatusOfThreads::CurrentWasherActionProgress = Time::TimeDiff(a, b) / 1000000000 * 100;
            }

            if(numberOfTable != -1)
            {
                //ma stolik
                StatusOfThreads::CurrentWasherAction = WasherAction::Eat;
                StatusOfThreads::CurrentWasherActionProgress = 0;

                uint aa = 10;
                for(int i = 0; i < aa; i++)
                {
                    StatusOfThreads::CurrentWasherActionProgress = ( i * 100 / aa );
                    std::this_thread::sleep_for(std::chrono::milliseconds( 3000  / aa));

                }

                StatusOfThreads::CurrentWasherActionProgress = 100;

                stat.mealsAmount++;
                StatusOfThreads::CurrentMealCounterOfWasher = stat.mealsAmount;

                //skoro ma gdzie odlozyc talerz, to dopiero teraz wstaje od stolika
                CommonResources::tablesMutex.lock();
                CommonResources::tables[numberOfTable] = -1;
                CommonResources::tablesMutex.unlock();

                //no i odklada talerz
                CommonResources::dirtyPlatesMutex.lock();
                CommonResources::dirtyPlatesAmount++;
                CommonResources::dirtyPlatesMutex.unlock();
            }
            else
            {
                //nie mam stolika, wiec odklada talerz
                CommonResources::dirtyPlatesMutex.lock();
                CommonResources::dirtyPlatesAmount++;
                CommonResources::dirtyPlatesMutex.unlock();
            }
        }

    }

}
