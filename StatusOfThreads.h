//
// Created by daniel on 02.06.19.
//

#ifndef ETAP_3_STATUSOFTHREADS_H
#define ETAP_3_STATUSOFTHREADS_H


#include <zconf.h>
#include <vector>
#include "Enum/CookAction.h"
#include "Enum/WasherAction.h"
#include "Enum/DinnerLadyAction.h"
#include "Enum/StudentAction.h"

class StatusOfThreads {
public:
    static CookAction CurrentCookAction;
    static uint CurrentCookActionProgress;

    static WasherAction CurrentWasherAction;
    static uint CurrentWasherActionProgress;
    static uint CurrentMealCounterOfWasher;

    static DinnerLadyAction CurrentDinnerLadyAction;

    static std::vector<StudentAction> CurrentActionOfStudents;
    static std::vector<uint> CurrentActionProgressOfStudents;
    static std::vector<uint> CurrentMealCounterOfStudents;

    static bool endKeyPressed;

};


#endif //ETAP_3_STATUSOFTHREADS_H
