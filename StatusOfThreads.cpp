//
// Created by daniel on 02.06.19.
//

#include "StatusOfThreads.h"

CookAction StatusOfThreads::CurrentCookAction;
uint StatusOfThreads::CurrentCookActionProgress;

WasherAction StatusOfThreads::CurrentWasherAction;
uint StatusOfThreads::CurrentWasherActionProgress;
uint StatusOfThreads::CurrentMealCounterOfWasher;

DinnerLadyAction StatusOfThreads::CurrentDinnerLadyAction;

std::vector<StudentAction> StatusOfThreads::CurrentActionOfStudents;
std::vector<uint> StatusOfThreads::CurrentActionProgressOfStudents;
std::vector<uint> StatusOfThreads::CurrentMealCounterOfStudents;


bool StatusOfThreads::endKeyPressed = false;


